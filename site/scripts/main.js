function sns_animate() {
    $(".sns a").hover(
        function() {
            var all_links = $(".sns a");
            for (var i = all_links.length - 1; i >= 0; i--) {
                if ($(all_links[i]).attr("href") != $(this).attr("href")){
                    $(all_links[i]).stop();
                    $(all_links[i]).fadeTo(500, 0.3);
                    $(all_links[i]).animate({
                        width: "40px",
                        height: "40px"
                    }, 500);
                }
            }
            $(this).stop();
            $(this).animate({
                width: "60px",
                height: "60px"
            }, 500);
        }, function() {
            var all_links = $(".sns a");
            for (var i = all_links.length - 1; i >= 0; i--) {
                if ($(all_links[i]).attr("href") != $(this).attr("href")){
                    $(all_links[i]).stop()
                    $(all_links[i]).css('opacity', 1);
                }
            }
            $(this).stop();
            $(this).css('opacity',1);
            $(this).css({
                width: "40px",
                height: "40px"
            });
    });
}

$(function() {
    sns_animate();
})